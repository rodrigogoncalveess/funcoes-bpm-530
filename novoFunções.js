const VERSAO_FUNCOES = '1.0.0';

/*******************************************************************************
 * --------------------------- VARIÁVEIS DE AMBIENTE ---------------------------
 ******************************************************************************/
const TITULO_ETAPA = ProcessData.activityTitle;
const COD_CICLO = ProcessData.cycle;
const COD_ETAPA = ProcessData.activityInstanceId;
const DOCUMENTO_STORE = Lecom.stores.DocumentStore;
// data de hoje com horário zerado
const HOJE = new Date(new Date().toDateString());

/*******************************************************************************
 * ------------------------ FUNÇÕES JS / REGRA DE TELA -------------------------
 ******************************************************************************/

// Informa que o arquivo foi carregado
(function () {
    $(document).ready(function () {
        console.log('Funções JS ' + VERSAO_FUNCOES + ' -- LOADED');
    });
})();

/**
 *  verfica se o email informado possui um padrão válido e insere um erro no
 * campo caso seja necessário
 * @param {String} nomeCampo Campo que será validado
 * @throws Campo não encontrado
 * @throws Campo não informado
 * @returns valor que deve estar no campo (email ou vazio)
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const validacaoEmail = function (nomeCampo) {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');

    const email = getValorCampo(nomeCampo);
    const REGEX = /^(([^<>()\[\]\.,;:\s@"]+(\.[^<>()\[\]\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const valido = REGEX.test(email);

    insereErroCampo(nomeCampo, 'Email informado não é válido.', !valido);
    return valido ? valor : '';
}

/**
 * Verifica se o CPF do campo informado é valido
 * @param {String} nomeCampo Campo que será validado
 * @throws Campo não encontrado
 * @throws Campo não informado
 * @returns Retorna o valor do campo. Caso seja inválido, é retornado
 * uma string vazia
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const validaCPF = function (nomeCampo) {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');

    const valorOriginal = getValorCampo(nomeCampo);
    let valido = true;
    const cpf = String(valorOriginal).match(/\d/g).join('');
    let aux = 0;
    // gera lista com 11 characteres repitindo numeros
    const invalido = Array(10).fill().map(function () {
        return String(aux++).repeat(11);
    });

    // elimina CPFs inválidos
    if (!cpf || cpf.length != 11 || invalido.includes(cpf)) valido = false;

    if (valido) {
        // verifica primeiro digito
        let soma = 0;
        for (i = 0; i < 9; i++) soma += Number(cpf[i] * (10 - i));

        let reverte = 11 - soma % 11;
        if (reverte == 10 || reverte == 11) reverte = 0;
        if (reverte != Number(cpf[9])) valido = false;
    }

    if (valido) {
        // verifica segundo digito
        soma = 0;
        for (i = 0; i < 10; i++) soma += Number(cpf[i] * (11 - i));

        reverte = 11 - soma % 11;
        if (reverte == 10 || reverte == 11) reverte = 0;
        if (reverte != Number(cpf[10])) valido = false;
    }

    insereErroCampo(nomeCampo, 'CPF informado não é válido.', !valido);

    return valido ? valorOriginal : '';
}

/**
 * Verifica se o CNPJ do campo informado é valido
 * @param {String} nomeCampo Campo que será validado
 * @throws Campo não encontrado
 * @throws Campo não informado
 * @returns Retorna o valor do campo. Caso seja inválido, é retornado
 * uma string vazia
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const validaCNPJ = function (nomeCampo) {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');

    const valorOriginal = getValorCampo(nomeCampo);
    let valido = true;
    const cnpj = String(valorOriginal).match(/\d/g).join('');
    let aux = 0;
    // gera lista com 14 characteres repitindo numeros
    const invalido = Array(10).fill().map(function () {
        return String(aux++).repeat(14);
    });

    // elimina cnpjs inválidos
    if (!cnpj || cnpj.length != 14 || invalido.includes(cnpj)) valido = false;

    // valida digito verificador
    let tamanho = cnpj.length - 2,
        numeros = cnpj.substring(0, tamanho),
        digitos = cnpj.substring(tamanho),
        soma = 0,
        pos = tamanho - 7;

    for (i = tamanho; i >= 1; i--) {
        soma += numeros[tamanho - i] * pos--;
        if (pos < 2) pos = 9;
    }

    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos[0]) valido = false;

    tamanho++;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;

    for (i = tamanho; i >= 1; i--) {
        soma += numeros[tamanho - i] * pos--;
        if (pos < 2) pos = 9;
    }

    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos[1]) valido = false;

    insereErroCampo(nomeCampo, 'CNPJ informado não é válido.', !valido);

    return valido ? valorOriginal : '';
}

/**
 * Valida se o PIS informado é valido
 * @param {String} nomeCampo Campo que será validado
 * @throws Campo não encontrado
 * @throws Campo não informado
 * @returns Retorna o valor do campo. Caso seja inválido, é retornado
 * uma string vazia
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const validaPIS = function (nomeCampo) {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');

    const valorOriginal = getValorCampo(nomeCampo);
    let valido = true;
    const pis = String(valorOriginal).match(/\d/g).join('');

    if (!pis)
        valido = false;

    let aux = 0;

    const invalido = Array(10).fill().map(function () {
        return String(aux++).repeat(11);
    });

    if (pis.length != 11 || invalido.includes(pis))
        valido = false;

    const peso = "3298765432";
    let total = 0;
    let resto = 0;

    for (i = 0; i <= 9; i++)
        total += (pis.slice(i, i + 1)) * (peso.slice(i, i + 1));

    resto = (total % 11);

    if (resto != 0)
        resto = 11 - resto;

    if (resto == 10 || resto == 11)
        resto = resto.toString().slice(1, 2);

    if (resto != (pis.slice(10, 11)))
        valido = false;

    insereErroCampo(nomeCampo, 'PIS informado não é válido.', !valido);
    return valido ? valorOriginal : '';
}

/**
 * Remove os valores de todos os campos e grids do formulário.
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const limpaFormulario = function () {
    getCampos().forEach(function (campo) {
        if (campo.type() === 'AUTOCOMPLETE')
            campo.addOptions('', true);
        else if (campo.fields)
            campo.dataRows().forEach(function (row) {
                getForm("ITEM").removeDataRow(row.id);
            });
        else
            campo.value('');
    });
}

/**
 * Valida se uma grid possui valores inseridos nela
 * @param {String|String[]} nomeGrid Grid que será validado
 * @throws Campo não encontrado
 * @throws Campo não informado
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const validaGridVazia = function (nomeGrid) {
    if (!nomeGrid)
        throw new Error('Por favor informe qual grid deve ser manipulado.');

    const grids = Array.isArray(nomeGrid) ? nomeGrid : nomeGrid.split(',');

    grids.forEach(function (nomGrid) {
        let vazio = false;
        const grid = getCampos(nomGrid);

        if (grid.visible())
            if (grid.dataRows().length === 0)
                vazio = true;

        insereErroCampo(nomeGrid, 'Por favor ensira pelo menos um valor na grid.', vazio);
    });

}

/*******************************************************************************
 * -------------------------- FUNÇÕES SÓMENTE PARA JS --------------------------
 ******************************************************************************/
/**
 * Simplificação Form.fields(), Form.grids().fields() 
 * e Form.grids().fields()
 * @param {String} nomeCampo (Opicional) - Nome do campo que deve ser
 * retornonado.
 * Sem valor retorna todos os campos e grids
 * @returns {Object|Object[]} Um ou todos os campos/grids da etapa
 * @throws Campo não encontrado
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const getCampos = function (nomeCampo) {
    nomeCampo = (nomeCampo || '').toString().trim();
    const todosCampos = Form.allFields();

    const retorno = (!!nomeCampo) ?
        todosCampos.find(function (campo) {
            return campo.id === nomeCampo;
        }) : todosCampos;

    if (!!retorno) return retorno;

    throw new Error('Campo ' + nomeCampo + ' não foi encontrado na Etapa.');
}

/**
 * Coleta valor do campo evitando retorno undefined ou de um array
 * @param {String} nomeCampo - Nome do campo de onde será pego o valor.
 * @returns {String} valor do campo;
 * @throws Campo não encontrado
 * @throws Campo não informado
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const getValorCampo = function (nomeCampo) {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');

    let valor = getCampos(nomeCampo).value();
    return !valor ? '' : Array.isArray(valor) ? valor.join('') : valor;
}

/**
 * Insere um valor em um campo
 * @param {String} nomeCampo - Nome do campo que deve ser retornonado.
 * @param {String} valor (Opicional) - Valor que será inserido no campo.
 * Padrão ''.
 * @returns {Object} Form para execução do apply caso necessário;
 * @throws Campo não encontrado
 * @throws Campo não informado
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const setValorCampo = function (nomeCampo, valor) {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');

    valor = valor === undefined ? '' : valor;

    return getCampos(nomeCampo).value(valor);
}

/**
 * Simplificação para configurar campo visivel ou invisivel.
 * Obs: já remove obrigatoridedade caso o campo seja configurado como invisivel
 * @param {String|String[]} campos - Um ou mais campos que serão configurados
 * @param {Boolean} visivel (Opcional) - true/false. Padrão true
 * @returns {Object} Form para execução do apply caso necessário;
 * @throws Campo não encontrado
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const setCamposVisivel = function (campos, visivel) {
    visivel = !!visivel;
    campos = Array.isArray(campos) ? campos : campos.split(',');

    campos.forEach(function (campo) {
        getCampos(campo).visible(visivel);
    });

    if (!visivel) {
        setCamposObrigatorio(campos, visivel);
    }

    return Form;
}

/**
 * Simplificação para configurar campo visivel ou invisivel.
 * Obs: já configura o campo como visivel caso o campo seja obrigatório
 * @param {String|String[]} campos - Um ou mais campos que serão configurados
 * @param {Boolean} obrigatoriedade (Opcional) - true/false. Padrão true
 * @param {String} acao (Opcional) - 'aprovar'/'rejeitar'. Padrão aprovar
 * @returns {Object} Form para execução do apply caso necessário;
 * @throws Campo não encontrado
 * @throws Campo não pode ser Obrigatório
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 * @todo FIXME - Verificar Caso de Botão gráfico e Botão de Aplicação
 */
const setCamposObrigatorio = function (campos, obrigatoriedade, acao) {
    obrigatoriedade = !!obrigatoriedade;
    campos = Array.isArray(campos) ? campos : campos.split(',');

    // Valida se algum campo informado não pode ser Obrigatório
    const erros = campos.filter(function (campo) {
        const objCampo = getCampos(campo);
        return objCampo.type() !== 'LABEL' || objCampo.type() !== 'GRID';
    });

    if (erros.length) {
        throw new Error('Campo(s) ' + erros.map(function (campo) {
            return campo.id
        }).join(', ') + ' Não podem ser obrigatórios');
    } else {
        campos.forEach(function (campo) {
            getCampos(campo).setRequired(acao, bool);
        });

        if (obrigatoriedade) {
            setCamposVisivel(campos, obrigatoriedade);
        }
        return Form;
    }
}

/**
 * Simplificação para configurar campo leitura ou normal.
 * Obs: já remove obrigatoridedade caso o campo seja configurado como leitura
 * @param {String|String[]} campos - Um ou mais campos que serão configurados
 * @param {Boolean} leitura (Opcional) - true/false. Padrão true
 * @returns {Object} Form para execução do apply caso necessário;
 * @throws Campo não encontrado
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const setCamposLeitura = function (campos, leitura) {
    leitura = !!leitura;
    campos = Array.isArray(campos) ? campos : campos.split(',');

    campos.forEach(function (valor) {
        getCampos(valor.trim()).readOnly(leitura);
    });

    if (leitura) {
        setCamposObrigatorio(campos, !leitura);
    }

    return Form;
}

/**
 * Simplificação para configurar campo bloqueado ou normal.
 * @param {String|String[]} campos - Um ou mais campos que serão configurados
 * @param {Boolean} bloquear (Opcional) - true/false. Padrão true
 * @returns {Object} Form para execução do apply caso necessário;
 * @throws Campo não encontrado
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const setCamposBloqueado = function (campos, bloquear) {
    bloquear = !!bloquear;
    campos = Array.isArray(campos) ? campos : campos.split(',');

    campos.forEach(function (valor) {
        getCampos(valor.trim()).disabled(bloquear);
    });

    return Form;
}

/**
 * Verifica se o campo está disponivel na etapa para ser manipulado
 * @param {String} nomeCampo
 * @returns true/false
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const campoExiste = function (nomeCampo) {
    try {
        getCampos(nomeCampo);
        return true;
    } catch (e) {
        return false;
    }
}

/**
 * Retorna em um array os objetos dos campos com o tipo informado
 * @param {String} tipoCampo TEXT, DATE, RADIO, DOCUMENT(anexo),
 * AUTOCOMPLETE(lista), CHECKBOX, TEXTAREA, LABEL
 * @returns {Object[]} array com o objeto dos campos com o tipo informado
 * @throws tipoCampo obrigatório
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const getCampossTipo = function (tipoCampo) {
    if (!tipoCampo)
        throw new Error('Por favor informe o tipo de campo. (TEXT, DATE, RADIO, DOCUMENT(anexo), AUTOCOMPLETE(lista), CHECKBOX, TEXTAREA, LABEL)');

    return getCampos().filter(function (campo) {
        return campo.type() === tipoCampo;
    });
}

/**
 * Cria um objeto do tipo data do valor informado
 * @param {String|Date} data - aceita um objeto date, uma string dd/mm/aaaa ou 
 * um array [dd, mm, aaaa]
 * @returns {Object} Objeto data
 * @throws Data obrigatória
 * @throws Data inválida
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const criarData = function (data) {
    if (!data)
        throw new Error('Por favor informe um objeto date, uma string dd/mm/aaaa ou um array[dd, mm, aaaa].');


    if (typeof data === 'object') return new Date(data);

    let dtFormatada = data.indexOf('/') > -1 ? data.split('/') : data;
    let dtFinal = dtFormatada;

    if (Array.isArray(dtFormatada))
        dtFinal = dtFormatada[1] + '/' + dtFormatada[0] + '/' + dtFormatada[2];

    const objData = new Date(dtFinal);

    if (objData.getDate() === NaN)
        throw new Error('Data inválida.');

    return objData;
}

/**
 * Converte uma data para o padrão desejado
 * @param {String|Date} data data a ser convertida
 * @param {String} formato (Opicional) - d -> dd/mm/yyyy, m -> mm/dd/yyyy,
 * a -> yyyy-mm-dd
 * @returns {String} data formatada de acordo com a escolha
 * @throws Data obrigatória
 * @throws Data inválida
 * @throws Formato inválido
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const criaDataString = function (data, formato) {
    if (!data)
        return "";

    formato = !!formato ? formato.trim().toLowerCase() : 'd';
    const objData = criarData(data);
    const dia = objData.getDate();
    let mes = objData.getMonth() + 1;
    mes = mes < 10 ? "0" + mes : mes;
    const ano = objData.getFullYear();

    if (formato === 'd')
        return dia + '/' + mes + '/' + ano;
    else if (formato === 'm')
        return mes + '/' + dia + '/' + ano;
    else if (formato === 'a')
        return ano + '-' + mes + '-' + dia;

    throw new Error('formato "' + formato + '" não é válido.');
}

/**
 * Adiciona ou remove uma mensagem de erro ao campo passado como parametro
 * @param {String|String[]} NomeDoCampo pode ser um unico campo, um array
 * de ou uma string com campos separados por virgula (,)
 * @param {String} mensagemErro Mensagem de Erro a ser manipulada
 * @param {Boolean} mostrarErro (Opicional) - se a mensagem deve ser
 * inserida (true) ou removida(false). Padrão true
 * @throws Campo não encontrado
 * @throws Campo não informado
 * @throws Mensagem de erro não informada
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const insereErroCampo = function (NomeDoCampo, mensagemErro, mostrarErro) {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');
    if (!mensagemErro)
        throw new Error('Por favor informe qual o erro.');

    mostrarErro = mostrarErro !== undefined ? mostrarErro : true;
    const campoForm = getCampos(NomeDoCampo);

    const errosCampo = campoForm.errors() || [];

    if (mostrarErro) {
        if (errosCampo.indexOf(mensagemErro) < 0) {
            errosCampo.push(mensagemErro);
            campoForm.errors(errosCampo);
        }
    } else {
        const index = errosCampo.indexOf(mensagemErro);
        if (index > -1) errosCampo.splice(index, 1);

        campoForm.errors(errosCampo);
    }
}

/**
 *  Transforma o conteudo de um campo em uma div
 * @param {String} nomeCampo Campo que será utilizado para gerar a div
 * @throws Campo não encontrado
 * @throws Campo não informado
 * @throws Mensagem de erro não informada
 * 
 * @author Rodrigo Silva
 * @since 12/07/2019
 */
const compilarHtml = function (nomeCampo) {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');

    if (getCampos(nomeCampo)) {
        $('#' + nomeCampo + '_div').remove();
        $('#input__' + nomeCampo).prepend('<div id = "' + nomeCampo + '_div">' +
            $('#' + nomeCampo).hide().val().replace(/'/g, '') + '</div>');
        $('#' + nomeCampo + '_div').css(' width ', ' 870 px ');
    }
}