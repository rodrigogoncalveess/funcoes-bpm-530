# Funções JS bpm 530

Conjunto de funções e variaveis de ambiente para simplificar o desenvolvimento 
de processo no bpm 5.30. Garantido funcionar a partir da RTM 1.16
<br /> Obs1: Para utilizar a versão encapsulada do funções (FuncoesClass) basta adicionar o js a etapa antes de qualquer outro js
e no js do processo, utilize FuncoesJS.FUNCAOX ou F.FUNCAOX, no funcoesJS basta chamar a função desejada
<br /> Obs2: Caso precise de funções genéricas expecificas para um cliente,
adicione em outro arquivo pra facilitar no suporte dessa classe
<br /> Obs3: Nenhuma função desse arquivo realiza Form.apply(). Fica a cargo do 
desenvolvedor escolher o momento certo para fazer isso.
<br /> Obs4: Para evitar conflitos, evite campos com o mesmo nome dentro e fora de grids

## Funções existentes:

* **getForm**(campo: string - opcional)
    * Retorona o objeto do campo informado, uma lista de todos os campos presentes na etapa se não informado o campo ou retorna um erro caso o campo não exista; 
* **valorCampo**(nomeCampo: string | string[], valor: string)
    * Pega valor de um campo evitando que o retorno seja undefined ou um array Caso o parametro "valor" seja passado, o mesmo é inserido no campo;
* **campoExiste**(nomeCampo: string)
    * Verifica se o campo pode ser pego pelo getForm / Form.fields, retornando true ou false ao invés de quebrar o código;
* **getCamposTipo**(tipoCampo: string)
    * Retorna um lista com todos os campos do tipo informado;
* **criarData**(data: string | Date)
    * Retorna um objeto Date com a data informada(dd / mm / yyyy);
* **criaDataString**(data: string | Date, padrao: string)
    * Converte uma data para o padrão desejado (dd/mm/yyyy, mm/dd/yyyy, yyyy-mm-dd)
* **validacaoEmail**(data: string | Date)
    * Verifica o texto em um campo é algo como email@provedor.algumaCoisa. Em caso negativo, informa um erro no campo;
* **exibeOcultaErro**(campo: string | string[], erroNovo: string, regra: boolean - opcional)
    * Adiciona ou remove uma mensagem de erro ao campo passado como parametro;
* **calculaDiffDias**(dataInicial: Date, dataFinal: Date)
    * Calcula quantidade de dias entre duas datas(dataFinal - dataInicial);
* **eventoValidarArquivo**(nomeCampo: string, extensao: string | string[], tamanho: string)
    * Cria um evento no campo para validar se o anexo segue as regras informadas;
* **validarArquivo**(file: any, campo: string, extensaoValida: string, tamanho: string)
    * Valida se o arquivo anexado possui as regras informadas;
* **campoVisivel**(nomeCampo: string | string[], show: boolean)
    * Abstração para setar campo visivel ou invisivel;
* **campoLeitura**(nomeCampo: string | string[], show: boolean)
    * Abstração para setar campo como somente leitura ou não;
* **campoBloqueado**(nomeCampo: string | string[], show: boolean)
    * Abstração para setar campo como bloqueado ou não;
* **campoObrigatorio**(nomeCampo: string | string[], bool: boolean, acao: string)
    * Abstração da validação de campo podendo receber apenas o campo a ser validado;
* **validaCPF**(cpf: string | number)
    * Verifica se o cpf informado é valido;
* **validaCNPJ**(cnpj: string | number)
    * Verifica se o cnpj informado é valido;
* **calculoGrid**(nomeGrid: string, funcao: Function)
    * Abstração para realizar calculos dentro de um grid sempre que os valores mudarem;
* **compilarHtml**(nomeCampo: string)
    * Insere o conteudo de um campo dentro de uma div, compilando o texto do campo em código html;
* **ValidaDocumentoAssinado**(nomeCampo: string)
    * Verifica se o arquivo dentro de um campo foi assinado digitalmente;
* **limparCampo**(nomeCampo: string | string[])
    * Remove todos os valores dentro de um campo;
* **botaoDownload**(campo: string, label: string)
    * Altera o botão de download do form por um mais visivel para o usuário;
* **contaDiasUteis**(dataInicial: string | Date, dataFinal: string | Date)
    * Retorna quantidade de dias uteis entre duas datas;
* **somaDiasUteis**(dataInicial: string | Date, qtdDias: number)
    * Retorna a data após somar a quantidade de dias informados a data informada;
* **calculaIdade**(dataNascimento: string | Date)
    * Retorna quantos anos se passaram desde a data informada;
* **limparGrupo**(idGrupo: String | Array)
    * Limpa conteudo dos campos dentro de um ou mais grupos;
* **obrigatoriedadeGrupo**(idGrupo: string | any[], bool: boolean, acao: string)
    * adiciona obrigatoriedade nos campos dentro de um ou mais grupos;
* **nulo**(input: any, retorno: string | number): any
    * Verifica as propriedades do input para retornar ou o própio input ou o retorno informado
* **modal**(titulo: string, mensagem: string): void
    * Modal simples do bpm com apenas um botão de confirmação
* **emAndamento**(): boolean
    * Verifica se a etapa está em andamento para executar algo
* **validaGridVazia**(nomeGrid: string): boolean
    * Caso a grid não possua linhas ou exista valores preechidos nos campos que não estão na grid o usuário será impedido de continuar com o processo
* **validaCamposInseridosGrid**(nomeGrid: string): boolean
    * Caso exista valores preechidos nos campos que não estão na grid o usuário será impedido de continuar com o processo
* **validaPIS**(pis: string): boolean
    * Valida se o PIS informado possui o padrão valido