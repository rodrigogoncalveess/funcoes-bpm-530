/**
 * @author       Rodrigo Silva
 * @since        Março/2019
 * @abstract Conjunto de funções e variaveis de ambiente para simplificar o 
 * desenvolvimento de processo no bpm 5.30. Garantido funcionar a partir da RTM 1.16
 * Obs1: Para utilizar basta adicionar o js a etapa antes de qualquer outro js e no 
 * js do processo, utilize FuncoesJS.FUNCAOX ou F.FUNCAOX
 * Obs2: Caso precise de funções genéricas expecificas para um cliente,
 * adicione em outro arquivo pra facilitar no suporte dessa classe
 * Obs3: Nenhuma função desse arquivo realiza Form.apply(). Fica a cargo do 
 * desenvolvedor escolher o momento certo para fazer isso.
 * Obs4: Para evitar conflitos, evite campos com o mesmo nome dentro e fora de grids
 */
FuncoesJS = {
    // Variaveis do bpm
    TITULO_ETAPA: ProcessData.activityTitle,
    COD_CICLO: ProcessData.cycle,
    COD_ETAPA: ProcessData.activityInstanceId,
    DOCUMENTO_STORE: Lecom.stores.DocumentStore,
    // data de hoje com horário zerado
    HOJE: new Date(new Date().toDateString()),

    /**
     * Simplificação Form.fields()/grids()/actions()
     * Obs: campos em grids não é necessário o nome da grid, o que impossibilita
     * de campos com mesmo nome dentro e fora de uma grid
     * @param {String} nomeCampo aceita 'CAMPO', 'botão' ou vazio
     */
    getForm: function (nomeCampo) {
        nomeCampo = nomeCampo ? nomeCampo.trim() : '';

        const forms = Form.fields().concat(Form.actions())
            .reduce(function (accu, curr) {
                return accu.concat(curr.fields ? curr.fields().concat(curr) : curr);
            }, []);

        const find = nomeCampo ? forms.find(function (form) {
            return form.id === nomeCampo;
        }) : forms;

        if (find) return find;

        throw new Error('Campo "' + nomeCampo + '" não existe na etapa atual ou no processo.');
    },

    /**
     * Pega valor de um campo evitando que o retorno seja undefined ou um array
     * Caso o parametro "valor" seja passado, o mesmo é inserido no campo
     * @param nomeCampo nome do campo a ser manipulado
     * @param valor valor a ser inserido no campo. OPICIONAL
     */
    valorCampo: function (nomeCampo, valor) {
        if (!nomeCampo)
            throw new Error('Por favor informe qual campo deve ser manipulado.');

        if (valor === undefined) {
            let valor = this.getForm(nomeCampo).value();
            valor = Array.isArray(valor) ? valor.join('') : valor

            return valor ? valor : '';
        }

        return this.getForm(nomeCampo).value(valor);
    },

    /**
     * Verifica se o campo está disponivel no formulário para ser manipulado
     * @param {String} nomeCampo
     */
    campoExiste: function (nomeCampo) {
        try {
            this.getForm(nomeCampo);
            return true;
        } catch (e) {
            return false;
        }
    },

    /**
     * Retorna em um array os objetos dos campos com o tipo informado
     * @param {String} tipoCampo TEXT, DATE, RADIO, DOCUMENT(anexo),
     * AUTOCOMPLETE(lista), CHECKBOX, TEXTAREA, LABEL
     * @returns {Object[]}
     */
    getCamposTipo: function (tipoCampo) {
        if (!tipoCampo)
            throw new Error('Por favor informe o tipo de campo. (TEXT, DATE, RADIO, DOCUMENT(anexo), AUTOCOMPLETE(lista), CHECKBOX, TEXTAREA, LABEL)');

        return this.getForm().filter(function (campo) {
            return campo.type() === tipoCampo;
        });
    },

    /**
     * Cria um objeto do tipo data do valor informado
     * @param {String|Date} data espera um objeto date, uma string dd/mm/aaaa ou 
     * um array [dd, mm, aaaa]
     * @returns {Object} Objeto data
     */
    criarData: function (data) {
        if (typeof data === 'object') return new Date(data);

        let dtFormatada = data.indexOf('/') > -1 ? data.split('/') : data;
        let dtFinal = dtFormatada;

        if (Array.isArray(dtFormatada))
            dtFinal = dtFormatada[1] + '/' + dtFormatada[0] + '/' + dtFormatada[2];

        const objData = new Date(dtFinal);

        if (objData.getDate() === NaN)
            throw new Error('Data inválida.');

        return objData;
    },

    /**
     * Converte uma data para o padrão desejado
     * @param {String|Date} data data a ser convertida
     * @param {String} padrao d - dd/mm/yyyy, m - mm/dd/yyyy, a - yyyy-mm-dd
     * @returns {String} data formatada de acordo com a escolha
     */
    criaDataString: function (data, padrao) {
        if (!data)
            return "";

        padrao = !!padrao ? padrao.trim().toLowerCase() : 'd';
        const objData = this.criarData(data);
        const dia = objData.getDate();
        let mes = objData.getMonth() + 1;
        mes = mes < 10 ? "0" + mes : mes;
        const ano = objData.getFullYear();

        if (padrao === 'd')
            return dia + '/' + mes + '/' + ano;
        else if (padrao === 'm')
            return mes + '/' + dia + '/' + ano;
        else if (padrao === 'a')
            return ano + '-' + mes + '-' + dia;

        throw new Error('padrão "' + padrao + '" não é um padrão válido.');
    },

    /**
     * Verfica se o email informado possui um padrão válido
     * @param {String} nomeCampo Campo que será validado
     * @returns {Boolean} resultado da validação
     */
    validacaoEmail: function (nomeCampo) {
        const email = this.valorCampo(nomeCampo);
        const REGEX = /^(([^<>()\[\]\.,;:\s@"]+(\.[^<>()\[\]\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return REGEX.test(email);
    },

    /**
     * Adiciona ou remove uma mensagem de erro ao campo passado como parametro
     * @param {String|String[]} campo pode ser um unico campo, um array
     * de ou uma string com campos separados por virgula (,)
     * @param {String} mensagemErro Mensagem de Erro a ser manipulada
     * @param {Boolean} mostrarErro se a mensagem deve ser inserida ou removida padrão = true
     * @returns {Object} objeto do campo contendo as alterações
     */
    exibeOcultaErro: function (NomeDoCampo, mensagemErro, mostrarErro) {
        mostrarErro = mostrarErro !== undefined ? mostrarErro : true;
        const campoForm = this.getForm(NomeDoCampo);

        const errosCampo = campoForm.errors() || [];

        if (mostrarErro) {
            if (errosCampo.indexOf(mensagemErro) < 0) {
                errosCampo.push(mensagemErro);
                campoForm.errors(errosCampo);
            }
        } else {
            const index = errosCampo.indexOf(mensagemErro);
            if (index > -1) errosCampo.splice(index, 1);

            campoForm.errors(errosCampo);
        }
        return campoForm;
    },

    /**
     * Calcula quantidade de dias entre duas datas(dataFinal - dataInicial)
     * @param {Date|String} dataInicial 
     * @param {Date|String} dataFinal 
     * @returns {Number} Diferença em dias entre duas datas
     */
    calculaDiffDias: function (dataInicial, dataFinal) {
        const ini = this.criaData(dataInicial);
        const fini = this.criaData(dataFinal);
        const timeDiff = fini - ini;

        return Math.round(timeDiff / (24 * 60 * 60 * 1000));
    },

    /**
     * Evento para validar campos do tipo DOCUMENT(anexo)
     * @param {String} nomeCampo nome do campo a ser validado
     * @param {String|String[]} extensao extensões permitidas (array ou separado por virgula)
     * @param {String} tamanho tamanho em Mb
     */
    eventoValidarArquivo: function (nomeCampo, extensao, tamanho) {
        const campo = Array.isArray(nomeCampo) ? nomeCampo : nomeCampo.split(',');

        campo.forEach(function (_campo) {
            getForm(_campo).subscribe('DOCUMENT_IMPORT_FILE_SUBMITTED',
                function (storeId, inputId, file) {
                    validarArquivo(file, _campo, extensao, tamanho);
                }
            );
        })
    },

    /**
     * Validação para arquivos nos templates com base em tamanho e tipo
     * @param {Object} file parametro do evento DOCUMENT_IMPORT_FILE_SUBMITTED
     * @param {String} campo nome do campo que será validado
     * @param {String} extensaoValida extensões permitidas separadas por virgula (,)
     * @param {String} tamanho tamanho em MB do arquivo
     */
    validarArquivo: function (file, campo, extensaoValida, tamanho) {
        let extensao = file.name.split(".");
        extensao = extensao[extensao.length - 1];

        this.DOCUMENTO_STORE.store[campo].flashMessage = null;

        const botaoImportar = $($('#documentModal .modal-footer-container .button-group a')[1]);
        botaoImportar.show();

        tamanho = tamanho ? tamanho : '';
        if (tamanho) {
            let tamanhoEmBytes = tamanho * Math.pow(10, 6);
            if (file.size > tamanhoEmBytes) {
                this.DOCUMENTO_STORE.store[campo].flashMessage = {
                    type: 'error',
                    message: 'Tamanho maior que o máximo permitido (' + tamanho + 'MB)'
                };
                botaoImportar.hide();
            }
        }

        extensaoValida = extensaoValida ? extensaoValida : '';
        if (extensaoValida) {
            extensaoValida = extensaoValida.split(',');
            if (extensaoValida.indexOf(extensao) < 0) {
                this.DOCUMENTO_STORE.store[campo].flashMessage = {
                    type: 'error',
                    message: 'Apenas arquivos do tipo ' + extensaoValida.join(', ') + ' são aceitos.'
                };
                botaoImportar.hide();
            }
        }
    },

    /**
     * Abstração para setar o campo como visivel ou invisivel
     * @param {String|String[]} nomeCampo pode ser um unico campo, um array de 
     * ou uma string com campos separados por virgula (,)
     * @param {Boolean} show opcional padrão true
     * @returns {Object} retorna o form permitindo campoVisivel(VALOR).apply();
     */
    campoVisivel: function (nomeCampo, show) {
        show = show !== undefined ? show : true;
        const campos = Array.isArray(nomeCampo) ? nomeCampo : nomeCampo.split(',');

        campos.forEach(function (campo) {
            this.getForm(campo.trim()).visible(show);
        });

        return Form;
    },

    /**
     * Abstração para setar campo como somente leitura ou não
     * @param {String|String[]} nomeCampo pode ser um unico campo, um array de 
     * ou uma string com campos separados por virgula (,)
     * @param {Boolean} show opcional padrão true
     * @returns {Object} retorna o form permitindo campoLeitura(VALOR).apply();
     */
    campoLeitura: function (nomeCampo, show) {
        show = show !== undefined ? show : true;
        campo = Array.isArray(nomeCampo) ? nomeCampo : nomeCampo.split(',');

        campo.forEach(function (valor) {
            this.getForm(valor.trim()).readOnly(show);
        });

        return Form;
    },

    /**
     * Abstração para setar campo como bloqueado ou não
     * @param {String|String[]} nomeCampo pode ser um unico campo, um array de 
     * ou uma string com campos separados por virgula (,)
     * @param {Boolean} show opcional padrão true
     * @returns {Object} retorna o form permitindo campoBloqueado(VALOR).apply();
     */
    campoBloqueado: function (nomeCampo, show) {
        show = show !== undefined ? show : true;
        campo = Array.isArray(nomeCampo) ? nomeCampo : nomeCampo.split(',');

        campo.forEach(function (valor) {
            getForm(valor.trim()).disabled(show);
        });

        return Form;
    },

    /**
     * Abstração da validação de campo podendo receber apenas o campo a ser validado
     * @param {String|String[]} nomeCampo pode ser um unico campo, um array de 
     * ou uma string com campos separados por virgula (,)
     * @param {boolean} bool obrigatório (true) ou não obrigatório (false)
     * padrão = true
     * @param {string} acao Ação em que o campo é obrigatório (aprovar/rejeitar)
     * padrão = aprovar
     * @returns {Object} retorna o form permitindo campoObrigatorio(VALOR).apply();
     */
    campoObrigatorio: function (nomeCampo, bool, acao) {
        bool = bool !== undefined ? bool : true;
        acao = acao || 'aprovar';
        nomeCampo = Array.isArray(nomeCampo) ? nomeCampo : nomeCampo.split(',');

        nomeCampo.forEach(function (campo) {
            const objCampo = this.getForm(campo);
            if (objCampo.type() !== 'LABEL' && !objCampo.hasOwnProperty('fields'))
                objCampo.setRequired(acao, bool);
        });

        return Form;
    },

    /**
     * Verifica se o cpf informado é valido
     * @param {String|Number} cpf numero do cpf (não é necessário limpar pontos)
     * @returns true ou false;
     */
    validaCPF: function (cpf) {
        cpf = String(cpf).match(/\d/g).join('');
        let aux = 0;
        // gera lista com 11 characteres repitindo numeros
        const invalido = Array(10).fill().map(function () {
            return String(aux++).repeat(11);
        });

        // elimina CPFs inválidos
        if (!cpf || cpf.length != 11 || invalido.includes(cpf)) return false;

        // verifica primeiro digito
        let soma = 0;
        for (i = 0; i < 9; i++) soma += Number(cpf[i] * (10 - i));

        let reverte = 11 - soma % 11;
        if (reverte == 10 || reverte == 11) reverte = 0;
        if (reverte != Number(cpf[9])) return false;

        // verifica segundo digito
        soma = 0;
        for (i = 0; i < 10; i++) soma += Number(cpf[i] * (11 - i));

        reverte = 11 - soma % 11;
        if (reverte == 10 || reverte == 11) reverte = 0;
        if (reverte != Number(cpf[10])) return false;

        return true;
    },

    /**
     * Verifica se o cnpj informado é valido
     * @param {String | Number} cnpj numero do cnpj(não é necessário limpar pontos) 
     * @returns true ou false;
     */
    validaCNPJ: function (cnpj) {
        cnpj = String(cnpj).match(/\d/g).join('');
        let aux = 0;
        // gera lista com 14 characteres repitindo numeros
        const invalido = Array(10).fill().map(function () {
            return String(aux++).repeat(14);
        });

        // elimina cnpjs inválidos
        if (!cnpj || cnpj.length != 14 || invalido.includes(cnpj)) return false;

        // valida digito verificador
        let tamanho = cnpj.length - 2,
            numeros = cnpj.substring(0, tamanho),
            digitos = cnpj.substring(tamanho),
            soma = 0,
            pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--) {
            soma += numeros[tamanho - i] * pos--;
            if (pos < 2) pos = 9;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos[0]) return false;

        tamanho++;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--) {
            soma += numeros[tamanho - i] * pos--;
            if (pos < 2) pos = 9;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos[1]) return false;

        return true;
    },

    /**
     * Abstração para realizar calculos dentro de um grid sempre que os valores 
     * mudarem
     * @param {String} nomeGrid Nome da grid onde ocorrerá o calculo
     * @param {Function} funcao Função que será executada quando algo na grid for
     * alterado (essa função deve receber as linhas da grid)
     */
    calculoGrid: function (nomeGrid, funcao) {
        const gridForm = this.getForm(nomeGrid);

        gridForm.subscribe('GRID_ADD_SUBMIT', function (ID, nCampo, campos) {
            const rows = gridForm.dataRows().concat(campos);
            funcao(rows);
        });

        gridForm.subscribe('GRID_EDIT_SUBMIT', function (ID, nCampo, campos) {
            const rows = gridForm.dataRows().filter(function (row) {
                return row.id !== campos.id;
            }).concat(campos);
            funcao(rows);
        });

        gridForm.subscribe('GRID_DESTROY', function (ID, n, n2, idRem) {
            const rows = gridForm.dataRows().filter(function (row) {
                return row.id !== idRem
            });
            funcao(rows);
        });
    },

    /**
     *  Transforma o conteudo de um campo em uma div
     * @param {String} nomeCampo nome do campo a ser compilado
     */
    compilarHtml: function (nomeCampo) {
        if (getForm(nomeCampo)) {
            $('#' + nomeCampo + '_div').remove();
            $('#input__' + nomeCampo).prepend('<div id = "' + nomeCampo + '_div">' +
                $('#' + nomeCampo).hide().val().replace(/'/g, '') + '</div>');
            $('#' + nomeCampo + '_div').css(' width ', ' 870 px ');
        }
    },

    /**
     * Verifica se o arquivo dentro de um campo foi assinado digitalmente
     * @param {String} nomeCampo campo a ser validado
     */
    validaDocumentoAssinado: function (nomeCampo) {
        const erro = 'Assinatura do documento é obrigatória.';
        let documentoAssinado = true;

        const valor = this.valorCampo(nomeCampo);
        if (valor) {
            const nomeArquivo = valor.split(":")[0],
                uniqueId = valor.split(":")[1],
                arquivoSplit = nomeArquivo.split('.'),
                extensao = arquivoSplit[arquivoSplit.length - 1];
            if (extensao.toLocaleUpperCase() == "PDF") {
                $.ajax({
                    type: 'post',
                    url: location.protocol + "//" + location.hostname + "/bpm/app/consultaDocumentosAssinados?action=validaDocumentoAssinado",
                    data: {
                        "uniqueId": uniqueId
                    },
                    success: function (retorno) {
                        if (retorno.sucesso != "true") {
                            documentoAssinado = false;
                        }
                    },
                    error: function (retorno) {
                        documentoAssinado = false;
                    },
                    async: false
                });
            } else {
                documentoAssinado = false;
            }
        }

        return this.exibeOcultaErro(campo, erro, !documentoAssinado);
    },

    /**
     * Remove todos os valores dentro de um campo
     * @param {String|String[]} nomeCampo nome do campo a ser alterado separado por
     * virgula (,) ou em um array
     * @returns {Object} retorna o form permitindo limparCampo(VALOR).apply();
     */
    limparCampo: function (nomeCampo) {
        nomeCampo = Array.isArray(nomeCampo) ? nomeCampo : nomeCampo.split(',');

        nomeCampo.forEach(function (campo) {
            if (this.getForm(campo).type() === 'AUTOCOMPLETE') {
                this.getForm(campo).addOptions('', true);
            } else {
                this.getForm(campo).value('');
            }
        });

        return Form;
    },

    /**
     * Altera o botão de download do form por um mais visivel para o usuário
     * @param {string} campo nome do campo onde será adicionado o botão
     * @param {string} label texto dentro do botão (máximo 12 caracteres)
     */
    botaoDownload: function (campo, label) {
        $('#botaoBaixarDoc').remove();
        $('#input__' + campo + ' .document-action-buttons').hide();
        $('#input__' + campo + ' .file-path-wrapper').before('<div class="form__button-group col s12 m12 l12 right-align" style="margin-left:0.5em;margin-right: 0.7em;" id="botaoBaixarDoc"><a id="baixarDoc" class="btn waves-effect waves-light" data-position="top" data-delay="10" data-tooltip="Baixar Arquivo"><span>' + label + '</span><i style="padding-top:0.7em" class="material-icons right">cloud_download</i></a><div>');

        $("#baixarDoc").click(function (e) {
            e.preventDefault();
            $('#input__' + campo + ' a')[0].click()
        });
    },

    /**
     * Retorna quantidade de dias uteis entre duas datas.
     * @param {String|Date} dataInicial data inicial da comparação
     * @param {String|Date} dataFinal  data final da comparação
     * @returns {Number} quantidade de dias uteis
     */
    contaDiasUteis: function (dataInicial, dataFinal) {
        if (dataInicial === dataFinal) return 0;
        let mult = 1,
            somaDias = 0,
            dtInicial = this.criarData(dataInicial),
            dtFinal = this.criarData(dataFinal);

        if (dtInicial > dtFinal) {
            mult = -1;
            const aux = dtInicial;
            dtInicial = dtFinal;
            dtFinal = aux;
        }

        while (dtInicial.getTime() < dtFinal.getTime()) {
            dtInicial.setDate(dtInicial.getDate() + 1);

            if ([0, 6].indexOf(dtInicial.getDay()) === -1)
                somaDias++;
        }

        return somaDias * mult;
    },

    /**
     * Retorna a data após somar a quantidade de dias informados a data informada
     * @param {String|Date} dataInicial data base
     * @param {Number} qtdDias Quantos dias uteis após dataInicial
     * @returns {Object} Objeto date com a data somada
     */
    somaDiasUteis: function (dataInicial, qtdDias) {

        const data = this.criarData(dataInicial);
        let i = 0;
        while (i < qtdDias) {
            if ([0, 6].indexOf(dtInicial.getDay()) === -1) {
                i++;
                data.setDate(data.getDate() + 1);
            }
        }

        return data;
    },

    /**
     * Retorna quantos anos se passaram desde a data informada
     * @param {String|Date} dataNascimento 
     * @returns {Number} idade
     */
    calculaIdade: function (dataNascimento) {
        return Math.floor(Math.abs(this.calculaDiferenca(this.HOJE, dataNascimento)) / 365);
    },

    /**
     * Limpa conteudo dos campos dentro de um ou mais grupos
     * @param {String|Array} idGrupo id de 1 ou mais grupos para serem limpos
     * @returns {Object} retorna o form permitindo limparGrupo(VALOR).apply(); 
     */
    limparGrupo: function (idGrupo) {
        idGrupo = Array.isArray(idGrupo) ? idGrupo : idGrupo.split(',');

        idGrupo.forEach(function (grupo) {
            Form.groups(grupo).fields().forEach(function (campo) {
                this.getForm(campo._id).value('');
            });
        });

        return Form;
    },

    /**
     * Adiciona obrigatoriedade nos campos dentro de um ou mais grupos
     * @param {String|Array} idGrupo id de 1 ou mais grupos para serem limpos
     * @param {boolean} bool obrigatório (true) ou não obrigatório (false)
     * padrão = true
     * @param {string} acao Ação em que o campo é obrigatório (aprovar/rejeitar)
     * padrão = aprovar
     * @returns {Object} retorna o form permitindo obrigatoriedadeGrupo(VALOR).apply(); 
     */
    obrigatoriedadeGrupo: function (idGrupo, bool, acao) {
        bool = bool !== undefined ? bool : true;
        acao = acao || 'aprovar';
        idGrupo = Array.isArray(idGrupo) ? idGrupo : idGrupo.split(',');
        idGrupo.forEach(function (grupo) {
            Form.groups(grupo).fields().forEach(function (campo) {
                const objCampo = this.getForm(campo._id);
                if (objCampo.type() !== 'LABEL' && !objCampo.hasOwnProperty('fields'))
                    objCampo.setRequired(acao, bool);
            });
        });

        return Form;
    },

    /**
     * Verifica as propriedades do input para retornar ou o própio input ou o
     * retorno informado
     * @param {String|Object} input
     * @param {String|Number} retorno 
     */
    nulo: function (teste, substituto) {
        if (teste || teste === 0)
            if (typeof teste === 'number' || typeof teste === 'string')
                return teste;
            else if (Object.keys(teste).length)
            return teste;
        return substituto;
    },

    /**
     * Modal simples do bpm com apenas um botão de confirmação
     * @param {String} titulo titulo da modal
     * @param {String} mensagem mensagem a ser exibida
     */
    modal: function (titulo, mensagem) {
        Form.addCustomModal({
            title: titulo,
            description: mensagem,
            showButtonClose: false,
            buttons: [{
                name: 'OK',
                icon: 'check',
                closeOnClick: true,
                action: function () {}
            }]
        });
    },

    /**
     * Verifica se a etapa está em andamento para executar algo
     * (bom para evitar alterações no formulário quando o usuario estiver 
     * visualizando uma etapa já executada)
     * @returns true caso a etapa esteja em andamento ou false
     */
    emAndamento: function () {
        return ProcessData.status === 'A';
    },

    /**
     * Caso a grid não possua linhas ou exista valores preechidos nos campos que
     * não estão na grid o usuário será impedido de continuar com o processo.
     * @param {String} nomeGrid 
     */
    validaGridVazia: function (nomeGrid) {
        let erro = false;
        const grids = Array.isArray(nomeGrid) ? nomeGrid : nomeGrid.split(',');

        grids.forEach(function (nomGrid) {
            if (Form.grids(nomGrid).visible())
                if (getForm(nomGrid).dataRows().length != 0)
                    erro = true;
        });

        return erro;
    },

    /**
     * Caso  exista valores preechidos nos campos que não estão na grid o
     * usuário será impedido de continuar com o processo.
     * @param {String} nomeGrid 
     */
    validaCamposInseridosGrid: function (nomeGrid) {
        let erro = false;
        const grids = Array.isArray(nomeGrid) ? nomeGrid : nomeGrid.split(',');

        grids.forEach(function (nomGrid) {
            const grid = getForm(nomGrid);
            if (grid.visible()) {
                grid.fields().forEach(function (campo) {
                    if (valorCampo(campo.id) && campo.visible() && !campo.disabled() && !campo.readOnly())
                        erro = true;
                });
            }
        });

        return erro;
    },

    /**
     * Valida se o PIS informado possui o padrão valido
     * @param {String} pis Valor a ser validado
     */
    validaPIS: function (pis) {
        pis = pis.replace(/[^\d]+/g, '');

        if (!pis)
            return false

        let aux = 0;

        const invalido = Array(10).fill().map(function () {
            return String(aux++).repeat(11);
        });

        if (pis.length != 11 || invalido.includes(pis))
            return false;

        const peso = "3298765432";
        let total = 0;
        let resto = 0;

        for (i = 0; i <= 9; i++)
            total += (pis.slice(i, i + 1)) * (peso.slice(i, i + 1));

        resto = (total % 11);

        if (resto != 0)
            resto = 11 - resto;

        if (resto == 10 || resto == 11)
            resto = resto.toString().slice(1, 2);

        if (resto != (pis.slice(10, 11)))
            return false;

        return true;
    }
}

/** Gera um alias para o objeto FuncoesJS */
let F;

(function () {
    $(document).ready(function () {
        F = FuncoesJS;
        console.log('FuncoesJS -- LOADED');
    });
})();